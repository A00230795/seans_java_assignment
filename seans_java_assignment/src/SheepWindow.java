import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class SheepWindow {

	public JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JLabel lblNamequantityOfRemedy;
	private JLabel lblIdentitiesOfAnimals;
	private JLabel lblWithdrawlPeriodExpiry;
	private JLabel lblSupplierOfRemedy;
	private JMenuBar menuBar;
	private JMenu mnNewMenu;
	private JMenuItem mntmMainMenu;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					SheepWindow window = new SheepWindow();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public SheepWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Sheep dosage records");
		frame.setBounds(100, 100, 1256, 404);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(28, 49, 124, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(179, 49, 174, 22);
		frame.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(389, 49, 124, 22);
		frame.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(552, 49, 162, 22);
		frame.getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(790, 49, 136, 22);
		frame.getContentPane().add(textField_4);
		
		JLabel lblDateOfAdministartion = new JLabel("Date of administartion");
		lblDateOfAdministartion.setBounds(28, 20, 152, 16);
		frame.getContentPane().add(lblDateOfAdministartion);
		
		lblNamequantityOfRemedy = new JLabel("Name/Quantity of remedy used");
		lblNamequantityOfRemedy.setBounds(179, 20, 184, 16);
		frame.getContentPane().add(lblNamequantityOfRemedy);
		
		lblIdentitiesOfAnimals = new JLabel("Identities of animals");
		lblIdentitiesOfAnimals.setBounds(390, 20, 152, 16);
		frame.getContentPane().add(lblIdentitiesOfAnimals);
		
		lblWithdrawlPeriodExpiry = new JLabel("Withdrawl period expiry date");
		lblWithdrawlPeriodExpiry.setBounds(552, 20, 174, 16);
		frame.getContentPane().add(lblWithdrawlPeriodExpiry);
		
		lblSupplierOfRemedy = new JLabel("Supplier of remedy");
		lblSupplierOfRemedy.setBounds(790, 20, 174, 16);
		frame.getContentPane().add(lblSupplierOfRemedy);
		
		menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		mnNewMenu = new JMenu("Options");
		menuBar.add(mnNewMenu);
		
		mntmMainMenu = new JMenuItem("Main Menu");
		mnNewMenu.add(mntmMainMenu);
		
		mntmMainMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpeningWindow openingWindow = new OpeningWindow();
				openingWindow.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
	}
}
