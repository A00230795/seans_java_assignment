class LinkedList {
	private Records head;
	private Cattle headCattle;

	public LinkedList() {
		head = null;
		headCattle = null;
	}

	public void insert(String d, String doseANDamt, String animalids, String wp, String remedysupplier) {
		Records temp = new Records(d, doseANDamt, animalids, wp, remedysupplier);
		temp.next = head;
		head = temp;
	}
	
	// prints the count of list items
	public int count() {
		Records temp = head;
		int res = 0;
		while (temp != null) {
			res++;
			temp = temp.next;
		}

		return res;
	}

	// would like this to print from the list based on the search
	public void search(String s1) {
		Records temp = head;

		while (temp != null) {
			if (temp.readAnimalsDosed().contains(s1)) 
			temp.print();
			temp = temp.next;	
			}
		System.out.println("\n \n");

	}
	
	
// deletes the first entry
	public String delete_first() {
		if (head == null)
			return null;
		String res = head.readDate();
		head = head.next;
		return res;
	}
	
	
// deletes all entries
	public void delete_all() {
		head = null;
	}

	// prints all the records
	public void printlist() {
		Records temp = head;
		System.out.println("\nList:");
		System.out.print("\n");
		System.out.print("HEAD->");
		System.out.print("\n");
		while (temp != null) {
			temp.print();
			System.out.print("\n \n");
			temp = temp.next;
		}

		System.out.print("NULL");

	}
	
	// prints all the records
		public void printlistCattle() {
			Cattle temp = headCattle;
			System.out.println("\nList:");
			System.out.print("\n");
			System.out.print("HEAD->");
			System.out.print("\n");
			while (temp != null) {
				temp.printCattle();
				System.out.print("\n \n");
				temp = temp.next;
			}

			System.out.print("NULL");

		}
		
		
		// Cattle stuff 
		
		// Hmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm 
		public void insertCattle(String id,int age) {
			Cattle temp = new Cattle(id,age);
			temp.next = headCattle;
			headCattle = temp;
		}
		
//		public  Cattle readCattle(int el)
//	    {      if (headCattle==null|| this.count()<el)
//	    	                 return null;
//			   Cattle temp=headCattle;
//	           int current=1;
//	           while(current!=el) {
//	                   current++;
//	                   temp=temp.next;}
//	           return temp; } 
		
		
		public  String readCattleNum(int el)
	    {     
			   Cattle temp=headCattle;
	           int current=0;
	           while(current!=el) {
	                   current++;
	                   temp=temp.next;
	                   }
	           return temp.getCattleNumber(); 
	  }
		
		
		// prints the count of cattle
		public int countCattle() {
			Cattle temp = headCattle;
			int res = 0;
			while (temp != null) {
				res++;
				temp = temp.next;
			}

			return res;
		}
	

}
