import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
public class AnimalSelection {
  public JFrame frame = new JFrame();
  JButton confirmselectBtn = new JButton("Confirm selection");
  JButton checkAllBtn = new JButton("Check All");
  JButton uncheckAllBtn = new JButton("UnCheck All");
  JButton cancelBtn = new JButton("Cancel");
  JPanel jPanel1 = new JPanel();
  JPanel jPanelSouth = new JPanel();
  JScrollPane jScrollPane1 = new JScrollPane(jPanel1);
  Checkbox checkbox ;
  Cattle cows;
  public  LinkedList cattleList = new LinkedList();
  public CattleWindow Cwindow=new CattleWindow();

  public AnimalSelection() {
	frame.setTitle("Choose Cattle");
    jPanel1.setLayout(new GridLayout(0, 2, 10, 10));
    jPanelSouth.setLayout(new GridLayout(1,3));
    ArrayList<Checkbox> alofCheckboxes = new ArrayList<Checkbox>();
    
    cattleList.insertCattle("0127", 7);
    cattleList.insertCattle("0137", 6);
    cattleList.insertCattle("0167", 3);
    
ArrayList<String> labels = new ArrayList<String>();
    
    
    for(int i=0; i<cattleList.countCattle(); i++)
    {
    	
    	labels.add(cattleList.readCattleNum(i));
    }
    
    for (int i = 0; i < labels.size(); i++) 
    {
      checkbox = new Checkbox(labels.get(i));
      alofCheckboxes.add(checkbox);
      jPanel1.add(checkbox);
      jPanel1.revalidate();
      jPanel1.repaint();
    }
    
    confirmselectBtn.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			ArrayList<String> infos = new ArrayList<String>();
			for (Checkbox checkBox : alofCheckboxes) {
			    if (checkBox.getState()) {
			        infos.add(checkBox.getLabel());
			    }
			}
			
			String listString="";
			for(String string:infos)
			listString += (String. valueOf(string)+ ", ");
			
			
			// HMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
			Cwindow.textField_2.setText(listString);
			System.out.println(listString);
		
		}
	});
    
    checkAllBtn.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			ArrayList<String> infos = new ArrayList<String>();
			for (Checkbox checkBox : alofCheckboxes) {
			   checkBox.setState(true);
			}	
		}
	});
    uncheckAllBtn.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			ArrayList<String> infos = new ArrayList<String>();
			for (Checkbox checkBox : alofCheckboxes) {
			   checkBox.setState(false);
			}	
		}
	});
    cancelBtn.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
		frame.setVisible(false);	
		}
	});
    
    frame.add(jScrollPane1);
    jPanelSouth.add(confirmselectBtn);
    jPanelSouth.add(checkAllBtn);
    jPanelSouth.add(uncheckAllBtn);
    jPanelSouth.add(cancelBtn);
    frame.add(jPanelSouth, BorderLayout.SOUTH);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(400, 300);
    frame.setVisible(true);
  }

  public static void main(String args[]) {
    new AnimalSelection();
  }
}