
public class Records {
	
	private String date, DoseANDAMT, AnimalsIDS,WithdrawalP,RemedySupplier;
	public Records next;
	
	public Records(String d,String doseANDamt,String animalids ,String wp,String remedysupplier)
	{
		date=d;
		DoseANDAMT=doseANDamt;
		AnimalsIDS=animalids;
		WithdrawalP=wp;
		RemedySupplier = remedysupplier;
	}
	
	public String readDate()
	{
		return date;
	}
	
	public String readDoseandAmount()
	{
		return DoseANDAMT;
	}
	
	public String readAnimalsDosed()
	{
		return AnimalsIDS;
	}
	
	public String readwithdrawalPeriod()
	{
		return WithdrawalP;
	}
	
	public String readremedySupplier()
	{
		return RemedySupplier;
	}
	
	public void print()
	{
		System.out.println("Date: "+ date);
		System.out.println("Dosage & Amount: "   + DoseANDAMT);
		System.out.println("Animals Dosed: "     + AnimalsIDS);
		System.out.println("Withdrawal period: " + WithdrawalP);
		System.out.println("Remedy Supplier: "   + RemedySupplier);
	}

}
