import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class OpeningWindow {

	public JFrame frame;
	private int index=0;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OpeningWindow window = new OpeningWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OpeningWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 853, 558);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTitle = new JLabel("Farm records application");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTitle.setBounds(230, 83, 340, 44);
		frame.getContentPane().add(lblTitle);
		
		JButton cattleButton = new JButton("Cattle remedies records");
		cattleButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CattleWindow cattleWindow = new CattleWindow();
				cattleWindow.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
		cattleButton.setBounds(290, 175, 192, 25);
		frame.getContentPane().add(cattleButton);
		
		JButton sheepButton = new JButton("Sheep remedies records");
		sheepButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SheepWindow sheepWindow = new SheepWindow();
				sheepWindow.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
		sheepButton.setBounds(290, 254, 192, 25);
		frame.getContentPane().add(sheepButton);
		
		JButton feedButton = new JButton("Feed purchases records");
		feedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FeedWindow feedWindow = new FeedWindow();
				feedWindow.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
		feedButton.setBounds(290, 328, 192, 25);
		frame.getContentPane().add(feedButton);
	}
	
	public void init_Records_list(){
		index=1;
		
	}
}
