public class Cattle {
   int animalsAge;
   String cattleNumber;
   public Cattle next;

   public Cattle(String ids,int age) {
      // This constructor has one parameter, ids.
	   cattleNumber = ids;
	   animalsAge = age;
   }

   public void setAge( int age ) {
      animalsAge = age;
   }

   public int getanimalsAge( ) {
      return animalsAge;
   }

   public String getCattleNumber() {
	   return cattleNumber;
   }
   
   public void printCattle() {
	   System.out.println("Cattle number: " + cattleNumber);
   }
   
   
}