import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class CattleWindow {
	public OpeningWindow owindow=new OpeningWindow();
	
	// THIS LINE FUCKS IT UP BIGTIME 
//	public AnimalSelection ASwindow=new AnimalSelection();

	public JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	public  JTextField textField_2;
	public JTextField textField_3;
	private JTextField textField_4;
	private JLabel     lblNamequantityOfRemedy;
	private JLabel     lblIdentitiesOfAnimals;
	private JLabel     lblWithdrawlPeriodExpiry;
	private JLabel     lblSupplierOfRemedy;
	private JMenuBar   menuBar;
	private JMenu      mnNewMenu;
	private JMenuItem  mntmMainMenu;
	private JButton    btnNewButton;
	private JButton    btnPrintAll;
	
	public int current = 0;
	public  LinkedList list = new LinkedList(); //This is new
	private JButton btnDeleteFirst;
	private JButton btnDeleteAll;
	private JButton btnListCount;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CattleWindow window = new CattleWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CattleWindow() {
		initialize();
		myRefresh();
	}
	
	public void myRefresh() {
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		list.insert("09/12/2018","pour on","0128,0125,0126,0129,108","28 days","AVC");
		list.insert("10/11/2017","Cobalt 10mg","0127","7 days","AVC"); //This is new
		list.insert("20/11/2017","Cobalt 10mg","0128","22 days","AVC");
		list.insert("12/11/2017","Cobalt 10mg","0129","20 days","AVC");
		list.insert("23/11/2017","Cobalt 10mg","0125","5 days","AVC");

		
		frame = new JFrame();
		frame.setTitle("Cattle remedy records");
		frame.setBounds(100, 100, 622, 647);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(210, 17, 174, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(210, 61, 174, 22);
		frame.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		//textField_2.setEditable(false);
		textField_2.setBounds(210, 113, 174, 22);
		frame.getContentPane().add(textField_2);
		
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(210, 166, 174, 22);
		frame.getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(210, 227, 174, 22);
		frame.getContentPane().add(textField_4);
		
		JLabel lblDateOfAdministartion = new JLabel("Date of administartion");
		lblDateOfAdministartion.setBounds(14, 20, 152, 16);
		frame.getContentPane().add(lblDateOfAdministartion);
		
		lblNamequantityOfRemedy = new JLabel("Name/Quantity of remedy used");
		lblNamequantityOfRemedy.setBounds(14, 64, 184, 16);
		frame.getContentPane().add(lblNamequantityOfRemedy);
		
		lblIdentitiesOfAnimals = new JLabel("Identities of animals");
		lblIdentitiesOfAnimals.setBounds(14, 116, 152, 16);
		frame.getContentPane().add(lblIdentitiesOfAnimals);
		
		lblWithdrawlPeriodExpiry = new JLabel("Withdrawl period expiry date");
		lblWithdrawlPeriodExpiry.setBounds(14, 169, 174, 16);
		frame.getContentPane().add(lblWithdrawlPeriodExpiry);
		
		lblSupplierOfRemedy = new JLabel("Supplier of remedy");
		lblSupplierOfRemedy.setBounds(14, 230, 174, 16);
		frame.getContentPane().add(lblSupplierOfRemedy);
		
		btnNewButton = new JButton("Add new entry");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String dateS=textField.getText();
                String remedyS=textField_1.getText();
                String animalsS=textField_2.getText();
                String wpS=textField_3.getText();
                String supplierS=textField_4.getText();
 		        list.insert(dateS, remedyS, animalsS,wpS,supplierS);
			}
		});
		btnNewButton.setBounds(14, 335, 124, 25);
		frame.getContentPane().add(btnNewButton);
		
		btnPrintAll = new JButton("Print all ");
		btnPrintAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				list.printlist();
			}
		});
		btnPrintAll.setBounds(150, 297, 126, 25);
		frame.getContentPane().add(btnPrintAll);
		
		JButton btnSearchAnimals = new JButton("Search Animals");
		btnSearchAnimals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String SEARCHINDATEFIELD=textField.getText();
				list.search(SEARCHINDATEFIELD);
			}
		});
		btnSearchAnimals.setBounds(14, 297, 124, 25);
		frame.getContentPane().add(btnSearchAnimals);
		
		btnDeleteFirst = new JButton("Delete First");
		btnDeleteFirst.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				list.delete_first();
				list.printlist();
				
			}
		});
		btnDeleteFirst.setBounds(150, 333, 124, 25);
		frame.getContentPane().add(btnDeleteFirst);
		
		btnDeleteAll = new JButton("Delete All");
		btnDeleteAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				list.delete_all();
				list.printlist();
			}
		});
		btnDeleteAll.setBounds(152, 371, 124, 25);
		frame.getContentPane().add(btnDeleteAll);
		
		btnListCount = new JButton("List Count");
		btnListCount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(list.count());
			}
		});
		btnListCount.setBounds(14, 373, 124, 25);
		frame.getContentPane().add(btnListCount);
		
		JButton btnSelectCattle = new JButton("Select cattle");
		btnSelectCattle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AnimalSelection window = new AnimalSelection();
		     	window.frame.setVisible(true);
			}
		});
		btnSelectCattle.setBounds(410, 112, 124, 25);
		frame.getContentPane().add(btnSelectCattle);
		
		menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		mnNewMenu = new JMenu("Options");
		menuBar.add(mnNewMenu);
		
		mntmMainMenu = new JMenuItem("Main menu");
		mnNewMenu.add(mntmMainMenu);
		
		mntmMainMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpeningWindow openingWindow = new OpeningWindow();
				openingWindow.frame.setVisible(true);
				frame.setVisible(false);
			}
		});
	}
	
	public void init_Records_list(){
		
		current=1;
		
	}
}